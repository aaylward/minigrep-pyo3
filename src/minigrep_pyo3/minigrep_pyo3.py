#===============================================================================
# minigrep_pyo3.py
#===============================================================================

# It is actually unnecessary to parse the args with Config, and in fact is
# inefficient because it requires an additional handoff between python and
# rust. The code is arranged this way for the sake of using Config as an
# example of a python class written in Rust with PyO3.

# Imports ======================================================================

from argparse import ArgumentParser
from minigrep_pyo3.version import __version__
from minigrep_pyo3.env import EXAMPLE_FILE, IGNORE_CASE
from minigrep_pyo3.minigrep_rust import Config, run



# Functions ====================================================================

def _run(args):
    for result in run(Config().build((args.query, args.content,
        str(IGNORE_CASE or args.case_insensitive)))):
        print(result)


def example_content(args):
    with open(EXAMPLE_FILE, 'r') as f:
        print(f.read(), end='')


def parse_arguments():
    parser = ArgumentParser(description='pyo3 implementation of minigrep')
    parser.add_argument('--version', action='version',
                    version='%(prog)s {version}'.format(version=__version__))
    subparsers = parser.add_subparsers(dest='func')
    run_parser = subparsers.add_parser('run', help='run minigrep')
    run_parser.set_defaults(func=_run)
    run_parser.add_argument('query', metavar='<query>', help='search query')
    run_parser.add_argument('content', metavar='<content.txt>', nargs='?',
                            default=EXAMPLE_FILE, help='file to search')
    case_group = run_parser.add_mutually_exclusive_group()
    case_group.add_argument('--insensitive', action='store_true',
                            dest='case_insensitive',
                            help='case-insensitive search')
    example_parser = subparsers.add_parser('example-content', help='view example content')
    example_parser.set_defaults(func=example_content)
    return parser.parse_args()
    

def main():
    args = parse_arguments()
    args.func(args)
