import os
import os.path

EXAMPLE_FILE = os.environ.get('MINIGREP_EXAMPLE_FILE',
    os.path.join(os.path.dirname(__file__), 'poem.txt'))
IGNORE_CASE = bool(os.environ.get('MINIGREP_IGNORE_CASE'))
