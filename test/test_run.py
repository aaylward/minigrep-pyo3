import pytest
import os.path

from minigrep_pyo3 import EXAMPLE_FILE, Config, run

@pytest.fixture
def poem_file():
    return EXAMPLE_FILE


@pytest.fixture
def case_sensitive_file():
    return os.path.join(os.path.dirname(__file__), 'data', 'case_sensitive.txt')


@pytest.fixture
def case_insensitive_file():
    return os.path.join(os.path.dirname(__file__), 'data', 'case_insensitive.txt')


def test_config_empty():
    config = Config()
    assert config.query == ''
    assert config.file_path == ''
    assert config.ignore_case == False


def test_config_frog(poem_file):
    config = Config().build(('frog', poem_file, 'False'))
    assert config.query == 'frog'
    assert config.file_path == poem_file
    assert config.ignore_case == False


def test_run(poem_file, case_sensitive_file, case_insensitive_file):
    assert run(Config().build(('frog', poem_file, 'False'))) == [
        'How public, like a frog']
    assert run(Config().build(('body', poem_file, 'False'))) == [
        "I'm nobody! Who are you?", "Are you nobody, too?",
        "How dreary to be somebody!"]
    assert run(Config().build(('monomorphization', poem_file, 'False'))) == []
    assert run(Config().build(('duct', case_sensitive_file, 'False'))) == [
        'safe, fast, productive.']
    assert run(Config().build(('rUsT', case_insensitive_file, 'True'))) == [
        'Rust:', 'Trust me.']
    assert run(Config().build(('to', poem_file, 'False'))) == [
        'Are you nobody, too?', 'How dreary to be somebody!']
    assert run(Config().build(('to', poem_file, 'True'))) == [
        'Are you nobody, too?', 'How dreary to be somebody!',
        'To tell your name the livelong day', 'To an admiring bog!']
