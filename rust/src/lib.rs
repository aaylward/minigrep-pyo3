use std::fs;
use pyo3::prelude::*;
use pyo3::exceptions::PyTypeError;

/// Configuration for a minigrep search
#[pyclass]
#[derive(Clone)]
struct Config {
    #[pyo3(get, set)]
    query: String,
    #[pyo3(get, set)]
    file_path: String,
    #[pyo3(get, set)]
    ignore_case: bool
}

#[pymethods]
impl Config {
    #[new]
    fn new() -> Self {
        return Config {
            query: String::new(),
            file_path: String::new(),
            ignore_case: false
        }
    }

    /// Build a new config object
    ///
    /// Parameters
    /// ----------
    /// args
    ///     iterable containing parameters of the search: query, filename,
    ///     ignore_case
    ///
    /// Returns
    /// -------
    /// Config
    ///     Config object with the provided parameters
    #[pyo3(text_signature = "(args)")]
    fn build(&self, args_vec: Vec<String>) -> PyResult<Config> {
        let mut args = args_vec.into_iter();
        let query = match args.next() {
            Some(arg) => arg,
            None => return Err(PyTypeError::new_err("Didn't get a query string"))
        };
        let file_path = match args.next() {
            Some(arg) => arg,
            None => return Err(PyTypeError::new_err("Didn't get a file path"))
        };
        let ignore_case = match args.next() {
            Some(arg) => arg == "True",
            None => return Err(PyTypeError::new_err("Didn't get sensitivity"))
        };
        Ok(Config { query, file_path, ignore_case })
    }
}

// Case-sensitive search
fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    contents.lines().filter(|line| line.contains(query)).collect()
}

// Case-insensitive search
fn search_case_insensitive<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let query = query.to_lowercase();
    let mut results = Vec::new();
    for line in contents.lines() {
        if line.to_lowercase().contains(&query) {
            results.push(line);
        }
    }
    results
}

/// Carry out a search on a file
///
/// Parameters
/// ----------
/// config: Config
///     a Config object containing parameters of the search: query, filename,
///     ignore_case
///
/// Returns
/// -------
/// list
///     list of lines containing the search query
#[pyfunction]
#[pyo3(text_signature = "(config)")]
fn run(config: Config) -> PyResult<Vec<String>> {
    let contents = fs::read_to_string(config.file_path)?;
    let results = if config.ignore_case {
        search_case_insensitive(&config.query, &contents)
    } else {
        search(&config.query, &contents)
    };
    let mut pyresults = Vec::new();
    for result in results { pyresults.push(String::from(result)); }
    Ok(pyresults)
}

#[pymodule]
fn minigrep_rust(_py:Python, m: &PyModule) ->PyResult<()> {
    m.add_class::<Config>()?;
    m.add_function(wrap_pyfunction!(run, m)?)?;
    Ok(())
}
