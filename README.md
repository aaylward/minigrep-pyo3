# minigrep-pyo3 documentation

## Overview

This package is a [PyO3](https://pyo3.rs/) implementation of the `minigrep` example project from chapter 12 of the [Rust Book](https://doc.rust-lang.org/stable/book/) . It is built with [maturin](https://www.maturin.rs/index.html) . It makes use of [pytest](https://docs.pytest.org/en/7.3.x/) for testing [Sphinx](https://www.sphinx-doc.org/en/master/), [GitLab CI](https://docs.gitlab.com/ee/ci/), and [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) for documentation.

## Installation

### From source

```sh
git clone https://gitlab.com/aaylward/minigrep-pyo3.git
cd minigrep-pyo3
pip install .
```

Check installation
------------------

Check that the installation was successful by running:

```sh
minigrep-pyo3 --version
```

See the available arguments by running `minigrep-pyo3 --help`:

```
usage: minigrep-pyo3 [-h] [--version] {run,example-content} ...

pyo3 implementation of minigrep

positional arguments:
  {run,example-content}
    run                 run minigrep
    example-content     view example content

options:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
```
