Tutorial
========

.. role:: zsh(code)
   :language: zsh

.. _example_content:

Example content
---------------

Use the :zsh:`example-content` subcommand to display the example content.

.. code-block:: zsh

   minigrep-pyo3 example-content

.. code-block:: none

   I'm nobody! Who are you?
   Are you nobody, too?
   Then there's a pair of us - don't tell!
   They'd banish us, you know.

   How dreary to be somebody!
   How public, like a frog
   To tell your name the livelong day
   To an admiring bog!

.. _search_file:

Search a file
-------------

Use the :zsh:`run` subcommand to perform a search. By default, the example
content is searched.

.. code-block:: zsh

   minigrep-pyo3 run frog

.. code-block:: none

   How public, like a frog

To search a different file, supply it as the second argument:

.. code-block:: zsh

   minigrep-pyo3 <query> <file.txt>
