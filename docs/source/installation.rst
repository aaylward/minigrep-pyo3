Installation
============

.. role:: zsh(code)
   :language: zsh

From source
-----------

.. code-block:: zsh

   git clone https://gitlab.com/aaylward/minigrep-pyo3.git
   cd minigrep-pyo3
   pip install .

Check installation
------------------

Check that the installation was successful by running:

.. code-block:: none

   minigrep-pyo3 --version

See the available arguments by running :zsh:`minigrep-pyo3 --help`:

.. code-block:: none

   usage: minigrep-pyo3 [-h] [--version] {run,example-content} ...

   pyo3 implementation of minigrep
   
   positional arguments:
     {run,example-content}
       run                 run minigrep
       example-content     view example content
   
   options:
     -h, --help            show this help message and exit
     --version             show program's version number and exit
