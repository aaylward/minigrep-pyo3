CLI reference
=============

.. role:: zsh(code)
   :language: zsh

run
---

.. code-block::  none

   usage: minigrep-pyo3 run [-h] [--insensitive] <query> [<content.txt>]

   positional arguments:
     <query>        search query
     <content.txt>  file to search

   options:
     -h, --help     show this help message and exit
     --insensitive  case-insensitive search


example-content
---------------

.. code-block:: none

   usage: minigrep-pyo3 example-content [-h]

   options:
     -h, --help  show this help message and exit
