API reference
=====================

.. autoclass:: minigrep_pyo3.Config
  :members:

.. autofunction:: minigrep_pyo3.run
